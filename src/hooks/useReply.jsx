import { v4 as uuidv4 } from "uuid";
import { useEffect, useState } from "react";

export default function useReply(replyObj = {}, onUpdate = () => {}) {
  const [parentReply, setParentReply] = useState(replyObj);

  function addReply(value) {
    const reply = {
      id: uuidv4(),
      value: value,
      replies: [],
    };

    setParentReply({
      ...parentReply,
      replies: [...parentReply.replies, reply],
    });
  }

  function deleteReply(replyId) {
    const replyIndex = setParentReply.replies.findIndex(
      (r) => r.id === replyId
    );
    const newReplies = [...setParentReply.replies];
    newReplies.splice(replyIndex, 1);
    setParentReply({
      ...parentReply,
      replies: newReplies,
    });
  }

  useEffect(() => {
    onUpdate(parentReply);
  }, [parentReply]);

  return {
    reply: parentReply,
    addReply,
    deleteReply,
  };
}
