import { useState } from "react";
import AddComment from './components/AddComment'
import Comment from "./components/Comment";


function App() {
  const [comments, setComments] = useState([])

  function addComment(comment) {
    setComments([...comments, comment])
  }

  function deleteComment(commentId) {
    const commentIndex = comments.findIndex(c => c.id === commentId)
    const newComments = [...comments]
    newComments.splice(commentIndex, 1)
    setComments(newComments)
  }

  function upadteComment(commentId, comment) {
    const commentIndex = comments.findIndex(c => c.id === commentId)
    const newComments = [...comments]
    newComments[commentIndex] = comment
    setComments(newComments)
  }

  console.log(comments)

  return (
    <div className="App">
      <AddComment addComment={addComment} />
      <div className="comments-list">
        {comments.map(c => (
          <Comment
            comment={c}
            key={c.id}
            deleteComment={deleteComment}
            upadteComment={upadteComment} />
        ))}
      </div>
    </div>
  );
}

export default App;
