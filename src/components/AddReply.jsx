import { useState } from "react";

export default function AddReply({ addReply }) {
  const [value, setValue] = useState("");
  function onChange(e) {
    setValue(e.target.value);
  }

  function onSubmit(e) {
    e.preventDefault();
    addReply(value);
    setValue("");
  }

  return (
    <div>
      <input value={value} onChange={onChange} />
      <button onClick={onSubmit}>Add Reply</button>
    </div>
  );
}
