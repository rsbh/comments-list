import { useState } from "react";
import { v4 as uuidv4 } from "uuid";

export default function AddComment({ addComment }) {
  const [value, setValue] = useState("");
  function onChange(e) {
    setValue(e.target.value);
  }

  function onSubmit(e) {
    e.preventDefault();
    addComment({
      id: uuidv4(),
      value: value,
      replies: [],
    });
    setValue("");
  }

  return (
    <div>
      <input value={value} onChange={onChange} />
      <button onClick={onSubmit}>Add Comment</button>
    </div>
  );
}
