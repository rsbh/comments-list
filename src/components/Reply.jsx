import { useState } from "react";
import AddReply from "./AddReply";
import { v4 as uuidv4 } from "uuid";

export default function Reply({
  reply,
  deleteReply,
  updateChild,
  nesting = 0,
}) {
  const [showReplyInput, setShowReplyInput] = useState(false);

  function onDelete() {
    deleteReply(reply.id);
  }

  function onReply() {
    setShowReplyInput(true);
  }

  function addReply(value) {
    const reply = {
      id: uuidv4(),
      value: value,
      replies: [],
    };
    updateChild(reply.id, {
      ...reply.replies,
      replies: [...reply.replies, reply],
    });
  }

  function deleteNestedReply(replyId) {
    const replyIndex = reply.replies.findIndex((r) => r.id === replyId);
    const newReplies = [...reply.replies];
    newReplies.splice(replyIndex, 1);
    updateChild(reply.id, {
      ...reply,
      replies: newReplies,
    });
  }

  function updateNestedChild(replyId, newReply) {
    const replyIndex = reply.replies.findIndex((r) => r.id === replyId);
    console.log(replyIndex, replyId);
    // const newReplies = [...reply.replies];
    // newReplies[replyIndex] = newReply;
    // updateChild(reply.id, {
    //   ...reply,
    //   replies: newReplies,
    // });
  }

  const marginLeft = nesting * 20;

  return (
    <div>
      <div>
        {reply.value}
        <button onClick={onDelete}>DELETE</button>
        <button onClick={onReply}>REPLY</button>
      </div>
      <div className="replies-list" style={{ marginLeft: marginLeft }}>
        {reply.replies.map((reply) => (
          <Reply
            key={reply.id}
            reply={reply}
            deleteReply={deleteNestedReply}
            updateChild={updateNestedChild}
            nesting={nesting + 1}
          />
        ))}
      </div>
      {showReplyInput && <AddReply addReply={addReply} />}
    </div>
  );
}
