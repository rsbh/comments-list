import { useState } from "react";
import AddReply from "./AddReply";
import { v4 as uuidv4 } from "uuid";
import Reply from "./Reply";

export default function Comment({
  comment,
  deleteComment,
  upadteComment,
  nesting = 0,
}) {
  const [showReplyInput, setShowReplyInput] = useState(false);

  function onDeleteComment() {
    deleteComment(comment.id);
  }

  function onReply() {
    setShowReplyInput(true);
  }

  function addReply(value) {
    const reply = {
      id: uuidv4(),
      value: value,
      replies: [],
    };

    upadteComment(comment.id, {
      ...comment,
      replies: [...comment.replies, reply],
    });
  }

  function deleteReply(replyId) {
    const replyIndex = comment.replies.findIndex((r) => r.id === replyId);
    const newReplies = [...comment.replies];
    newReplies.splice(replyIndex, 1);
    upadteComment(comment.id, {
      ...comment,
      replies: newReplies,
    });
  }

  function updateChild(replyId, newReply) {
    console.log(replyId, newReply);
    const replyIndex = comment.replies.findIndex((r) => r.id === replyId);
    const newReplies = [...comment.replies];
    newReplies[replyIndex] = newReply;
    upadteComment(comment.id, {
      ...comment,
      replies: newReplies,
    });
  }

  return (
    <div className="comment">
      <div>
        {comment.value}
        <button onClick={onDeleteComment}>DELETE</button>
        <button onClick={onReply}>REPLY</button>
      </div>
      <div className="replies">
        {comment.replies.map((reply) => (
          <Reply
            key={reply.id}
            reply={reply}
            deleteReply={deleteReply}
            updateChild={updateChild}
            nesting={nesting + 1}
          />
        ))}
      </div>
      {showReplyInput && <AddReply addReply={addReply} />}
    </div>
  );
}
